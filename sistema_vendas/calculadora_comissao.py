import math

PERCENTUAL_COMISSAO_FAIXA_2 = 0.06

PERCENTUAL_COMISSAO_FAIXA_1 = 0.05

VALOR_LIMITE_FAIXA_1 = 10000

def calcular(valor_venda):


    if valor_venda <= VALOR_LIMITE_FAIXA_1:
        return math.floor(valor_venda * PERCENTUAL_COMISSAO_FAIXA_1 * 100) / 100
    else:
        return valor_venda * PERCENTUAL_COMISSAO_FAIXA_2
