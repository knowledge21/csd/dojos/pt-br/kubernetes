# Dojo de Kubernetes

Este projeto contém o estado final do dojo básico de kubernetes como parte do conteúdo de Entrega Contínua (CD) dos treinamentos **Certified Scrum Developer** e **Testes Automatizados** da Knowledge21.

O estado final está na branch _master_, e o estado inicial na branch _start_.

## Requisitos

O projeto é uma API com um cálculo simples feita em Python (nosso Dojo de testes de unidade), mas como o objetivo é brincar um pouco mais com a cultura DevOps, a linguagem em que foi desenvolvido é quase que indiferente. Esteja a vontade para experimentar em outras linguagens, já que o rolê está conteinerizado.

Já que estamos falando de conteiners, é importante você [ter o docker instalado](https://docs.docker.com/engine/install/) aí no seu computador pra brincadeira ser mais divertida.

É interessante também você já ter rodado os Dojos de Testes de Unidade e de Integração Contínua (CI) com a gente para ter mais contexto da calculadora e de Docker básico. Mas se não rodou, tá suave tb.

### Criação do Cluster k8s (kubernetes) na Digital Ocean

Para rodar esse Dojo você **vai precisar** de um Cluster criado em algum lugar. Pode ser na Amazon, Google Cloud Platform ou qualquer outra nuvem (só toma cuidado com o Azure que eles podem te cobrar milhares de reais sem você nem saber, hihi).

Recomendamos fortemente você rodar o nosso [Dojo de Terraform](https://gitlab.com/knowledge21/csd/dojos/pt-br/terraform) para criar esse cluster diretamente na Digital Ocean só com código usando o Terraform (com a famosa infrastructure as code). Tenta lá, é suave!

Depois de criar o Cluster na sua conta da Digital Ocean, siga os passos desse tutorial que a própria Digital Ocean vai te indicar pra conseguir conectar seu compudador com o Cluster que está na nuvem e utilizar os comandos através do ```kubectl```

[![digital-ocean-kubernetes-tutorial.png](https://i.postimg.cc/x1wYR0yn/digital-ocean-kubernetes-tutorial.png)](https://postimg.cc/TKJZ2v44)

:)

### Para rodar na sua máquina antes de brincar com Kubernetes

Abra o projeto na sua IDE de Python favorita (ou no VSCode mesmo, hihi) e manda esse comando na raíz do projeto:

```docker build -t calculadorinha-show-de-bola .```

Pra rodar o conteiner na sua máquina a partir da imagem criada o comando é o seguinte:

```docker run -p 8088:8000 calculadorinha-show-de-bola```

Pra acessar a calculadora é só abrir na sua máquina a URL

```http://localhost:8088/comissao/55.59```

Sucesso, né? :P


## Desafio

O desafio desse Dojo é você subir esse mesmo conteiner num cluster Kubernetes utilizando o pipeline do próprio Gitlab.

Faça um fork desse repo, utilize a branch _start_ (estado inicial do Dojo) e mande bala! 

Se quiser, pode usar a branch _master_ como referência, afinal o Dojo já está resolvido nela.

Boa sorte! :)
